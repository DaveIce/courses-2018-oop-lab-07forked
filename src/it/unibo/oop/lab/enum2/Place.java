package it.unibo.oop.lab.enum2;

public enum Place {
	INDOOR("Indoor"),OUTDOOR("Outdoor");


private final String actualName;

private Place(final String name) {
	this.actualName = name;
}

public String toString() {
	return this.actualName;
}


}